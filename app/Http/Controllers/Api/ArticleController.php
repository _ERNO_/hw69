<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $articles = Article::paginate(5);
        return ArticleResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleStoreRequest $request)
    {
        $this->authorize('create', Article::class);
        $data = $request->validated();
        $data['user_id'] = auth('api')->user()->getAuthIdentifier();
        $article = Article::create($data);
        return response($article, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return ArticleResource
     */
    public function show(Article $article)
    {
        $this->authorize('view', $article);
        return new ArticleResource($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleStoreRequest $request
     * @param \App\Models\Article $article
     * @return ArticleResource
     */
    public function update(ArticleStoreRequest $request, Article $article)
    {
        $this->authorize('update', $article);
        $article->update($request->validated());
        $article->fresh();
        return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Article $article)
    {
        $this->authorize('delete', $article);
        $article->delete();
        return response([], 204);
    }
}
