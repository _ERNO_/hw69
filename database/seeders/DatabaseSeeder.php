<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $users = User::factory(10)->create();
         $users->each(function($user) {
            $articles = Article::factory()->count(5)->for($user)->create();
            $articles->each(function ($article){
               Comment::factory()->count(rand(2, 4))->for($article)->create();
            });
         });
    }
}
