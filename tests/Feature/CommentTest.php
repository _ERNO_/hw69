<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_success_get_all_comments()
    {
        $user = User::factory()->create();
        Passport::actingAs($user, [route('comments.index')]);
        $article = Article::factory()->for($user)->create();
       $comments = Comment::factory()->for($user)->for($article)->count(2)->create();
       $response = $this->getJson(route('comments.index'));
       $response->assertOk();
       $response->assertJsonStructure([
            'data',
           'meta' => [
               'current_page',
               'links',
               'from',
               'last_page',
               'path',
               'per_page',
               'to',
               'total',
           ],
           'links'
       ]);
       $this->assertCount($comments->count(), $response->json()['data']);
    }

    /**
     * @group comment
     * @return void
     */
    public function test_not_aut_user_trying_get_all_comments()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        Comment::factory()->for($user)->for($article)->create();
        $response = $this->getJson(route('comments.index'));
        $response->assertUnauthorized();
    }

    /**
     * @group comment
     * @return void
     */
    public function test_not_auth_user_trying_get_once_comment()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        $response = $this->getJson(route('comments.show', compact('comment')));
        $response->assertUnauthorized();
    }



    /**
     * @group comment
     * @return void
     */
    public function test_user_success_get_once_comment()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        Passport::actingAs($user, [route('comments.show', compact('comment'))]);
        $response = $this->getJson(route('comments.show', compact('comment')));
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                'id',
                'body',
                'article' => [
                    'id',
                    'title',
                    'content'
                ],
                'user' => [
                    'id',
                    'email'
                ],
            ],
        ]);
        $this->assertEquals($comment->id, $response->json()['data']['id']);
        $this->assertEquals($comment->body, $response->json()['data']['body']);
        $this->assertEquals($comment->user_id, $response->json()['data']['user']['id']);
        $this->assertEquals($comment->article_id, $response->json()['data']['article']['id']);
    }

    /**
     * @group comment
     * @return void
     */
    public function test_user_success_delete_comment()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        Passport::actingAs($user, [route('comments.destroy', compact('comment'))]);
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'id' => (string) $comment->id,
            'user_id' => (string) $comment->user_id,
            'article_id' => (string) $comment->article_id,
        ]);
        $response = $this->deleteJson(route('comments.destroy', compact('comment')));
        $response->assertNoContent();
        $this->assertDatabaseMissing('comments', [
            'body' => $comment->body,
            'id' => (string) $comment->id,
            'user_id' => (string) $comment->user_id,
            'article_id' => (string) $comment->article_id,
        ]);
    }


    /**
     * @group comment
     * @return void
     */
    public function test_user_can_not_delete_not_his_comment()
    {
        $user = User::factory()->create();
        $another_user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        Passport::actingAs($another_user, [route('comments.destroy', compact('comment'))]);
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'id' => (string) $comment->id,
            'user_id' => (string) $comment->user_id,
            'article_id' => (string) $comment->article_id,
        ]);
        $response = $this->deleteJson(route('comments.destroy', compact('comment')));
        $response->assertForbidden();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'id' => (string) $comment->id,
            'user_id' => (string) $comment->user_id,
            'article_id' => (string) $comment->article_id,
        ]);
    }


    /**
     * @group comment
     * @return void
     */
    public function test_not_auth_user_can_not_delete_comment()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'id' => (string) $comment->id,
            'user_id' => (string) $comment->user_id,
            'article_id' => (string) $comment->article_id,
        ]);
        $response = $this->deleteJson(route('comments.destroy', compact('comment')));
        $response->assertUnauthorized();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'id' => (string) $comment->id,
            'user_id' => (string) $comment->user_id,
            'article_id' => (string) $comment->article_id,
        ]);
    }


    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_auth_not_user_can_not_create()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $response = $this->postJson(route('comments.store'), ['body' => 'Some body', 'user_id' => $user->id, 'article_id' => $article->id]);
        $response->assertUnauthorized();
    }



    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_auth_user_success_comment_create()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        Passport::actingAs($user, [route('comments.create', compact('article'))]);
        $response = $this->postJson(route('comments.create', compact('article')),
            [
                'body' => 'Some body',
                'article_id' => $article->id,
                'user_id' => $user->id,
            ]);
        $response->assertStatus(201);
    }


    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_auth_user_success_comment_update()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        Passport::actingAs($user, [route('comments.update', compact('comment'))]);
        $response = $this->putJson(route('comments.update', compact('comment')),
            [
                'body' => 'Some 2body',
                'article_id' => $article->id,
                'user_id' => $user->id,
            ]);
        $response->assertStatus(200);
    }


    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_auth_not_user_can_not_comment_update()
    {
        $user = User::factory()->create();
        $article = Article::factory()->for($user)->create();
        $comment = Comment::factory()->for($user)->for($article)->create();
        $response = $this->putJson(route('comments.update', compact('comment')),
            [
                'body' => 'Some 2body',
                'article_id' => $article->id,
                'user_id' => $user->id,
            ]);
        $response->assertUnauthorized();
    }
}


